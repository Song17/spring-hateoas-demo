package com.twuc.hateoasdemo;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

// varargs
// endpointclose pro
// https://gitlab.com/twu-c-trainee/course/spring-test/03-3-custom-response-hateoas.git


@RestController
@RequestMapping("/api")
public class EmployeeController {

    private EmployeeRepository repository;
    private EmployeeResourceAssembler assembler;

    public EmployeeController(EmployeeRepository repository,
                              EmployeeResourceAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/employees/{id}")
    public Resource<Employee> one(@PathVariable String id) {
        Employee employee = repository.findById(id);
        return new Resource<>(employee,
                linkTo(methodOn(EmployeeController.class).one(id)).withSelfRel());
    }

    @GetMapping("/employees")
    public Resources<Resource<Employee>> all() {

        List<Resource<Employee>> resources = repository.findAll().stream()
                .map(e -> assembler.toResource(e))
                .collect(Collectors.toList());

        return new Resources<>(resources,
                linkTo(methodOn(EmployeeController.class).all()).withSelfRel());
    }

    @PostMapping("/employees")
    public ResponseEntity create(@RequestBody Employee employee) throws URISyntaxException {
        Employee savedEmployee = this.repository.save(employee);
        return ResponseEntity
                .status(HttpStatus.CREATED.value())
                .location(new URI(assembler.toResource(savedEmployee).getId().expand().getHref()))
                .build();
    }

}
